# Задание 3
# 1) Создайте UNIX сокет, который принимает сообщение с двумя числами, разделенными запятой.
# 2) Сервер должен конвертировать строковое сообщения в два числа и вычислять его сумму.
# 3) После успешного вычисления возвращать ответ клиенту.

import os
import socket

unix_sock_name = 'unix.sock'
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

if os.path.exists(unix_sock_name):
    os.remove(unix_sock_name)

sock.bind(unix_sock_name)

while True:
    try:
        result = sock.recv(1024)
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        print('sum = ', result.decode('utf-8'))
