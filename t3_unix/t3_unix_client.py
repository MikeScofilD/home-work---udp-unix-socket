# Задание 3
# 1) Создайте UNIX сокет, который принимает сообщение с двумя числами, разделенными запятой.
# 2) Сервер должен конвертировать строковое сообщения в два числа и вычислять его сумму.
# 3) После успешного вычисления возвращать ответ клиенту.

import socket
import json

a = 5
b = 7
res = json.dumps(a + b).encode()
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
sock.sendto(res, 'unix.sock')