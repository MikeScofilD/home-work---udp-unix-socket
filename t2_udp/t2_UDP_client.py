# Задание 2
# 1)Создайте UDP сервер, который ожидает сообщения о новых устройствах в сети.
# 2)Он принимает сообщения определенного формата,
#   в котором будет идентификатор устройства и печатает новые подключения в консоль.
# 3)Создайте UDP клиента, который будет отправлять уникальный
#   идентификатор устройства на сервер, уведомляя о своем присутствии.

import socket
import uuid
import json

# 3) Печатаем идентификатор устройства
uID = str(uuid.uuid4())
uID_json = json.dumps(uID).encode()
print(f"device ID: {uID_json}")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(uID_json, ('localhost', 8888))

